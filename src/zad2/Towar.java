package zad2;

public class Towar {
	private String index;
	private int waga;

	public Towar(String string, int i) {
		setIndex(string);
		setWaga(i);
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public int getWaga() {
		return waga;
	}

	public void setWaga(int waga) {
		this.waga = waga;
	}
	
	

}
