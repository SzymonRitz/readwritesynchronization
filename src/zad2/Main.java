/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		final Main m = new Main();
		ArrayList<Towar> towary = new ArrayList<Towar>();
		Thread pierwszy = new Thread(new Runnable() {

			@Override
			public void run() {
				synchronized (m) {
					BufferedReader br = null;
					try {
						int i = 0;
						String towar;
						br = new BufferedReader(new FileReader("../Towary.txt"));
						
						while ((towar = br.readLine()) != null) {
							m.notify();
							towary.add(new Towar(towar.split(" ")[0], Integer.parseInt(towar.split(" ")[1])));
							if (i % 200 == 0 && i != 0) {
								System.out.println("Utworzono " + i + " obiekt�w");
								i++;
							}
							i++;
							try {
								m.wait();
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						try {
							if (br != null)
								br.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
					}
					if (!Thread.currentThread().isInterrupted())
						m.notify();
				}
			}
		});

		pierwszy.start();
		Thread drugi = new Thread(new Runnable() {

			@Override
			public void run() {
				int masa = 0;
				synchronized (m) {
					for (int i = 0; i < towary.size(); i++) {
						m.notify();
						masa += towary.get(i).getWaga();
						if (i % 100 == 0)
							System.out.println("policzono mase " + i);

						try {
							m.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				System.out.println("Masa wynosi " + masa);
			}
		});

		drugi.start();
	}
}
