package zad1;

public class StringTask implements Runnable {

	TaskState wartosc;
	String calosc = "";
	int ilerazy;
	String litera = "";
	Thread watek;
	boolean koniec = false;

	public StringTask(String string, int i) {
		watek = new Thread(this);
		litera = string;
		ilerazy = i;
		wartosc = TaskState.CREATED;

	}

	public TaskState getState() {

		return wartosc;
	}

	public void start() {
		watek.start();

	}

	public boolean isDone() {

		return koniec;
	}

	public String getResult() {

		return calosc;
	}

	@Override
	public void run() {
		wartosc = TaskState.RUNNING;
		for (int i = 0; i < ilerazy; i++) {
			calosc += litera;
		}
		koniec = true;
		wartosc = TaskState.READY;
	}

	public void abort() {
		Thread.currentThread().interrupt();
		koniec = true;
		wartosc = TaskState.ABORTED;

	}

}
